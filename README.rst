:skip-help: true
:css: assets/style.css

.. title:: Introducción a TypeScript
.. footer: Globant México, Junio 2017


build

.. code::

    hovercraft README.rst dist/

----


TypeScript
==========

Una introducción
----------------

.. code:: typescript

    const by = {
        name: 'Alberto Maturano',
        email: 'jose.maturano@globant.com',
        position: 'WebUI Developer'
    };

----


Agenda:
-------

- ¿Qué es TypeScript?

- ¿Qué características tiene?

- ¿Cómo podríamos implementarlo?

----



.. epigraph::

   "JavaScript es bien raro. Yo en Java [...]"

   -- Conversación casual en los pasillos de Globant


----


TypeScript!
-----------

- Código JavaScript Escalable; Modular y Typado

- Curva de aprendizaje baja desarrolladores de otros lenguajes (Java o C#)

- No-invasiva (existing libs, browser support)

- Visión a largo plazo

- Código JS producido claro y manejable

.. note::

  - Sigue sobre el trabajo que determina futuras versiones de EcmaScript

----


¿Qué es?
--------

- Lenguaje libre de programación escrito en TypeScript

- Creado por Microsoft en 2012

- Sintaxis similar a C# / Java

- Sin intérprete especial, sin librerías particulares

- "Superset de JavaScript"

- Ha ganado popularidad en recientes años


.. note::

    - Licencia Apache 2

    - Como una capa que facilita la programación orientada a objetos

    - MS aboga porque lenguajes y herramientas deben trabajar juntas para crear la mejor experiencia para el desarrollo

    - Usa toda librería disponible por JS (NPM)

    - https://insights.stackoverflow.com/survey/2017#technology-most-loved-dreaded-and-wanted-languages 


----

.. image:: assets/ts_venn.png
    :width: 100 %
    :align: center

"TypeScript comienza y termina con JavaScript"

----


ES2015
------

.. include:: code/es2015.ts
    :code: typescript
    :number-lines: 1

----


ES2015 - Classes
----------------

.. include:: code/clases.ts
    :code: typescript
    :number-lines: 1

----


TypeScript
----------

  - Lenguaje Opcionalmente Typado

  - Interfaces, ENUMS, Types

  - Generics


----


Interfaces
----------

.. include:: code/interface.ts
    :code: typescript
    :number-lines: 1

.. note::

    Sólo para el compilador. No general código JS

----

.

----



Tipos de datos - Básicos
------------------------

.. include:: code/type_basic.ts
    :code: typescript
    :number-lines: 1

----

.

----


Tipos de datos - Básicos (2)
----------------------------

.. include:: code/type_basic_2.ts
    :code: typescript
    :number-lines: 1

.. note::

    - void: usualmente para funciones que no regresan valor

    - null y undefined son subtipos de otros. Por eso se puede asignar

    - Con `--strictNullChecks` falla si no se especifíca explícitamente puede
      tomar esos valores

    - Excepcion, error, o nunca alcanza un endpoint

----


Tipos de datos - Tuples
-----------------------

.. include:: code/type_tuple.ts
    :code: typescript
    :number-lines: 1

----

.

----


Tipos de datos - Enums
----------------------

.. include:: code/type_enum.ts
    :code: typescript
    :number-lines: 1

.. note::

    - Trabajar de manera amigable y segura con valores numéricos

----


Tipos de datos - Assertions (Cast)
-------------------------------------

.. include:: code/type_assertion.ts
    :code: typescript
    :number-lines: 1

.. note::
    Lección aprendida: sólo es en tiempo de desarrollo

----


¿Cómo implementarlo?
--------------------

----

Renombrar
---------

.. code:: bash

    find -type f -name '*.js' -exec \
        bash -c 'mv "${1}" "${1%???}.ts"' bash {} \;


----


Compilar
--------

.. code:: bash

    tsc --init


.. code:: json

    {
        "compilerOptions": {
            "outDir": "./built",
            "allowJs": true,
            "target": "es5"
        },
        "include": [
            "./src/**/*"
        ]
    }

----


Compilar
--------

- `--allwaysStrict`

- `--declaration`

- `--module`

- `--noEmitOnError`

- `--noImplicitAny`

- `--outFile`

- `--sourceMap`

- `--watch`

----


Agregar tipado
--------------
    
    - Nuestro propio código

    - De terceros | https://microsoft.github.io/TypeSearch/

.. code:: bash

    npm install @types/node

----


Refactorizar
------------

.. code:: javascript

    // Node/CommonJS code:
    var foo = require("foo");

    foo.doStuff();

    // RequireJS/AMD code:
    define(["foo"], function(foo) {
        foo.doStuff();
    })

---

.. code:: typescript

    import foo = require("foo");

    foo.doStuff();

----


FAQ
---

    - `gulp-typescript`

    - `awesome-typescript-loader` | `source-map-loader`

    - `grunt-typescript`


----


Comentarios?
-------------

