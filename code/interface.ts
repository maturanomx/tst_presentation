interface ICustomer {
    firstName: string;
    lastName: string;
    age?: number; // Atributo opcional
}

class Customer implements ICustomer {
    public firstName;
    public lastName;
}

let person: ICustomer = { firstName: 'John', lastName: 'Doe' };

service().then((data: ICustomer) => {
    // ...
});
