// Modules
import returnObject from 'somelib';

// let + const
let bob = {
    name: 'Bob',
    // Arrow functions, default params
    greeting: (greet = 'Hello!, My name is') => {
        // Template strings, Lexical this
        console.log(`${greet} ${this._name}`)
    }
}

// Destructuring, Rest, Spread
let { sortHand, foo: bar, ...rest } = returnObject();

