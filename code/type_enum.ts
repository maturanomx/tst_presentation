enum Color { Red, Green, Blue }

function selectColor(selected: Color): void {
    if (Color.Red) { // ... }
}

selectColor('White'); // ERROR


type Easing = 'ease-in' | 'ease-out' | 'ease-in-out';

function ease(easing: Easing) {
    if (easing === 'ease-in') { // ...  }
}

ease('uneasy'); // ERROR

