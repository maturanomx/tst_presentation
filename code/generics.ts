import ICustomer from './generics';

export class List<T> {
    add(item: T) {
        // ...
    }
}

let custs = new List<ICustomer>();
custs.add({ firstName: 'John', lastName: 'Doe' });
