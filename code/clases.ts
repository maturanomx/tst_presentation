class Auto {
    constructor(private _engine) {}

    set engine(val) {
        this._engine = val;
    }

    get engine() {
        return this._engine;
    }

    start() {
        console.log('Take off using: ', + this._engine);
    }
}