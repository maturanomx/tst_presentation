let isValid: boolean = false;

let years: number = 18;

let username: string = 'john';

let friends: string[] = ['foo', 'bar'];
// let friends: Array<string>;

let dinamic: any = 'this var could be assigned any value';
// No errors
dinamic = true; 
dinamic = { foo: 'bar' };

let list: any[] = [1, true, 'free'];
list[1] = 100;